# Useful Notebooks


## STPT and AXIO Notebooks

* Export STPT dataset to TIFF as one file per slide, optical offset and channel. [export_as_tiff_singlechannels.ipynb](stpt/export_as_tiff_singlechannels.ipynb)
* Export a single slice of an STPT dataset to OME-TIFF to read in QuPath or Fiji. [export_as_ome_tiff.ipynb](stpt/export_as_ome_tiff.ipynb)
* Access a remote dataset from your local computer. It also shows how to save the full datasets or some slices locally. [access_zarr_remotely.ipynb](stpt/access_zarr_remotely.ipynb)
* Notes on running the pipeline. [running_imaxt_mosaic_pipline.ipynb](stpt/running_imaxt_mosaic_pipline.ipynb)
* Quality Control bead analysis notebook. [qc_beads_analysis.ipynb](stpt/qc_beads_analysis.ipynb)

## IMC Notebooks

 * Flatfield correction of IMC panorama images. [flatfield_imc_panorama.ipynb](imc/flatfield_imc_panorama.ipynb)


# Tutorials

* Connect to JupyterHub from Visual Studio Code running in your desktop computer. [jupyter_from_vscode.ipynb](tutorials/jupyter_from_vscode.ipynb)
* Submitting a pipeline from the command line using the Owl client.  [submitting_pipeline_owl.ipynb](tutorials/submitting_pipeline_owl.ipynb)

